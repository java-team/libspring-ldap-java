#!/bin/sh 

set -e

# called by uscan with '--upstream-version' <version> <file>
echo "version $2"
upstream_package="spring-ldap"
package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
debian_version=`dpkg-parsechangelog | sed -ne 's/^Version: \(.*\)-.*/\1/p'`
DOCS_URL="https://src.springframework.org/svn/spring-ldap/tags/spring-ldap-$2/src/docbkx/"
TAR=${package}_${debian_version}.orig.tar.gz
DIR=${package}-${debian_version}.orig

# clean up the upstream sources
unzip $3 && mv ${upstream_package}-$2 $DIR
svn export $DOCS_URL $DIR/docs/docbkx
GZIP=--best tar --numeric --group 0 --owner 0 --anchored \
   -X debian/orig-tar.excludes -c -v -z -f $TAR $DIR
rm -rf $3 $DIR
