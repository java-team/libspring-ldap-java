Source: libspring-ldap-java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <miguel@miguel.cc>, James Page <james.page@ubuntu.com>
Section: java
Priority: optional
Build-Depends: ant,
               ant-optional,
               debhelper (>= 9),
               default-jdk,
               default-jdk-doc,
               fop,
               javacc,
               javahelper,
               libcommons-lang-java,
               libcommons-logging-java,
               libcommons-pool-java,
               libspring-beans-java,
               libspring-context-java,
               libspring-core-java,
               libspring-jdbc-java,
               libspring-orm-java,
               libspring-transaction-java,
               maven-repo-helper (>= 1.5~),
               xmlto
Standards-Version: 3.9.5
Vcs-Git: git://anonscm.debian.org/pkg-java/libspring-ldap-java.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-java/libspring-ldap-java.git
Homepage: http://www.springsource.org/ldap

Package: libspring-ldap-java
Architecture: all
Depends: libcommons-lang-java,
         libcommons-logging-java,
         libspring-beans-java,
         libspring-core-java,
         ${misc:Depends}
Recommends: libspring-jdbc-java, libspring-transaction-java
Suggests: libcommons-pool-java, libspring-context-java, libspring-ldap-java-doc
Description: Java library for simpler LDAP programming
 Spring LDAP is built on the same principles as the JdbcTemplate in Spring
 JDBC. It completely eliminates the need to worry about creating and closing
 LdapContext and looping through NamingEnumeration. It also provides a
 more comprehensive unchecked Exception hierarchy, built on Spring's
 DataAccessException. As a bonus, it also contains classes for dynamically
 building LDAP filters and DNs (Distinguished Names), LDAP attribute
 management, and client-side LDAP transaction management.

Package: libspring-ldap-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: default-jdk-doc, libspring-ldap-java (= ${binary:Version})
Description: documentation for libspring-ldap-java
 Documentation for Spring LDAP, that is a Java library built on the same
 principles as the JdbcTemplate in Spring JDBC. It completely eliminates the
 need to worry about creating and closing LdapContext and looping through
 NamingEnumeration.
